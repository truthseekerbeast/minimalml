## Stats topics



#### Statistical approach

Steps contained in statistical approach are,

* **Data collection**: Collect all necessary data files 
* **Descriptive statistics**: Summarize data concisely and Visualize data.
* **Exploratory data analysis**: look for patterns, differences, and
  other features that address the questions we are interested in.
* **Estimation:** Estimate characteristics
* **Hypothesis testing:** Where we see apparent eects, like a dierence
  between two groups, we will evaluate whether the eect might have
  happened by chance.



#### Pandas

The python library we will use for data representation and manipulation.

Pandas **Dataframes** are structure same as excel sheets, they are 2 dimensional, *Row  and Columns*

Every column of DataFrame is a **Series** which is another base Data structure in pandas.

Detailed description is provided in pandas section: [HERE](../pandas.md)



#### Frequency 

How many times value appears in a particular dataset.

Most simple way to represent Frequencies for each values is Histogram, It is representation of frequencies for each values appears in dataset.

  

#### Distributions

One of the best ways to describe a variable is to report the **values that appear**
**in the dataset and how many times each value appears**. This description is
called the distribution of the variable.

```
Tip : When you start working with a new dataset, I suggest you explore the vari-
ables you are planning to use one at a time,
```


**Histograms is a graph that shows the frequency of each value.**

Histograms are useful because they make the most frequent values immediately apparent. But they are not the best choice for comparing two distributions.



#### Outliers 

which are extreme values that might be errors in measurement and recording, or might be accurate reports of rare events.

```
Tip : The best way to handle outliers depends on domain knowledge that is,
information about where the data come from and what they mean. And it
depends on what analysis you are planning to perform.
```



#### Some important data characteristics 

| Characteristic   | Description                                                  |                                         |
| ---------------- | ------------------------------------------------------------ | --------------------------------------- |
| central tendency | Do the values tend to cluster around a particular point?     | **Mean** describes the central tendency |
| modes            | Is there more than one cluster?                              | Mode                                    |
| spread           | How much variability is there in the values?                 | Variance                                |
| tails            | How quickly do the probabilities drop off as we move away from the modes? | -                                       |
| outliers         | Are there extreme values far from the modes?                 | -                                       |



##### Mean

Sum of the values, Divided by number of values.

```
Example when mean is useful

Apples are all pretty much the same size (at least the ones sold in supermar-
kets). So if I buy 6 apples and the total weight is 3 pounds, it would be a
reasonable summary to say they are about a half pound each.
```



##### Variance

Need of variance:

```
But pumpkins are more diverse. Suppose I grow several varieties in my gar-
den, and one day I harvest three decorative pumpkins that are 1 pound each,
two pie pumpkins that are 3 pounds each, and one Atlantic Giant pumpkin
that weighs 591 pounds. The mean of this sample is 100 pounds, but if I
told you. The average pumpkin in my garden is 100 pounds," that would be
misleading. In this example, there is no meaningful average because there is
no typical pumpkin.
```



![](images/variance_formula.PNG)

The term *xi - x(bar)* is called the "**deviation from the mean**," so **variance is the**
**mean squared deviation**. 

**The square root of variance S is the standard deviation.**

*Variance is useful in some calculations, but it is not a good summary statistic.*



##### Effect size

-> A summary statistic intended to quantify the size of an effect like a difference between groups.

```
For example, to describe the difference between two groups,
one obvious choice is the difference in the means.
Mean pregnancy length for first babies is 38.601; for other babies it is 38.523.
The difference is 0.078 weeks, which works out to 13 hours. As a fraction of
the typical pregnancy length, this difference is about 0.2%.
If we assume this estimate is accurate, such a difference would have no prac-
tical consequences.
```

So in above scenario effect size is very small and have no practical impact.

Cohen's d is a statistic intended to measure Effect size, which can be calculated by,

![](images/effect_size.PNG)

*where x1 and x2 are mean of two groups and s is "Pooled standard deviation"*

![](images/effect_size_table.PNG)

*Above table summarize the values associated with effect sizes*



#### Probability mass functions.

Another way to represent distribution is by plotting probabilities instead of Frequencies.

**Probability -> Frequency of Any Value in sample / Sample size**

To get from Frequencies to Probabilities we divided by n, which is called normalization 


