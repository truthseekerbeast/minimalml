

Note : This topics were referred from [Khan Academy playlist](https://www.khanacademy.org/math/statistics-probability) 

'' * '' represents More useful links

------



# Analyzing categorical data



### Data that has only 1 categorical variable



1. [Exercise](https://www.khanacademy.org/math/statistics-probability/analyzing-categorical-data/modal/v/identifying-individuals-variables-and-categorical-variables-in-a-data-set) to Identify Individuals, Variables and Categorical variables from Dataset.

   ![](images/individual_var_cat_car.PNG)

2. Mean, Median, Mode, MidRange, Range [Exercies](https://www.khanacademy.org/math/statistics-probability/analyzing-categorical-data/modal/v/reading-bar-charts-3)

   **Mean** : Sum of all values / Total number of values

   **Median** : Sort all the values and take middle value from the range.

    * If Range has middle value like 1 ,2 ,3, 4, 5 has middle value 3
    * If Range is like 1, 2, 3, 4 then it will be mean of 2 middle values (2+3) / 2 -> 2.5



   **Mode** : Most Repeated or Common value in data.

   **Range** : (Maximum value - Minimum value) of data

   **Mid Range** : (Maximum value + Minimum Value) / 2 



### Data that has 2 categorical variables 



#### Two way tables 

*Two-way tables organize data based on **two categorical variables**.*

[2 way tables and Venn diagrams](https://www.khanacademy.org/math/statistics-probability/analyzing-categorical-data/modal/v/two-way-frequency-tables-and-venn-diagrams) for representation of Information - Basics



*Two-way relative **frequency tables show us percentages rather than counts**. They are good for seeing if there is an association between two variables.*

[2 way Relative Frequencies](https://www.khanacademy.org/math/statistics-probability/analyzing-categorical-data/modal/v/two-way-relative-frequency-tables) table *

Analyzing [Trends](https://www.khanacademy.org/math/statistics-probability/analyzing-categorical-data/modal/v/analyzing-trends-categorical-data) *



[Nice review notes of two way tables](https://www.khanacademy.org/math/statistics-probability/analyzing-categorical-data/modal/a/two-way-tables-review)

[Marginal and Conditional distributions](https://www.khanacademy.org/math/statistics-probability/analyzing-categorical-data/modal/v/marginal-distribution-and-conditional-distribution) of 2 way tables *



# Quantitative data and it's representation techniques

[Histograms](https://www.khanacademy.org/math/statistics-probability/displaying-describing-data/modal/v/histograms-intro)

[Stem and leaf plot](https://www.khanacademy.org/math/statistics-probability/displaying-describing-data/modal/a/stem-and-leaf-plots-review)

![](images/stem-leaf-plot.PNG)

[Comparisons](https://www.khanacademy.org/math/statistics-probability/displaying-describing-data/modal/v/comparing-dot-plots-histograms-and-box-plots) between dot plots, histograms and box plots  

[Line graphs](https://www.khanacademy.org/math/statistics-probability/displaying-describing-data/modal/v/u08-l1-t2-we2-reading-line-graphs)



# Summarizing quantitative data.

### Mean, Median and Mode 

The distribution types and values of mean, median and mode with them.

![](images/mean_median_mode_with_tailed_distribution.PNG)

In simplest terms these 3 can be described as below,

**Mean** : Sum of all values in distributions / number of distributions

**Median** : Middle value of sorted distribution, if it has no middle value then we will take two middle values and take mean of them.

**Mode** : Most repeated value in distribution



[Statistics intro and Mean, Median and Mode](https://www.khanacademy.org/math/statistics-probability/summarizing-quantitative-data/modal/v/statistics-intro-mean-median-and-mode)

```
Example of Which should be used to define central tendency or 'typical value' in Distribution? 

-> A golf team's 6 members had the scores below in their most recent tournament:
70,72,74,76,80,114

Mean would be 81
Where as Median would be (74 + 76)/2 = 75

Here, this distribution has an outlier and that is 114. Due to it Mean becomes 81, 

which is much higher then all the other values, and thus not a good statistics to describe this distribution.

But, Median is 75. Which is far better representation.

So, We need to predict as per scenarios and values, which fits the best to data.

```

[Review and Practice for Mean, Median and Mode](https://www.khanacademy.org/math/statistics-probability/summarizing-quantitative-data/modal/a/mean-median-and-mode-review)



### Interquartile ranges

Explained simply with example [here](https://www.khanacademy.org/math/statistics-probability/summarizing-quantitative-data/modal/v/calculating-interquartile-range-iqr)

```
Example:
Data : 10,20, 40, 70, 90, 120 ,140
Step 1: sort the data and find median
-> This data is already sorted and median is 70.

Step 2: Now divide the data in 2 groups, from median
-> Group 1: 10,20,40 and Group 2: 90,120,140

Step 3 : Calculate median of those.
-> Group 1: 20 Group 2 : 120

Step 4 : IQ range is difference between Medians of upper group of number and Lower group of numbers.
-> 120 - 20 = 100

```





### Variance and Standard deviation of population


**Deviation** is simply how far is something ?, when someone says deviation from the mean they say how far that value is from mean?

[Calculation of Variance and standard deviation of population](https://www.khanacademy.org/math/statistics-probability/summarizing-quantitative-data/modal/v/range-variance-and-standard-deviation-as-measures-of-dispersion) 

```
-*- Calculation -*-

Step 1 : Find the mean
Step 2 : Find all of the data point's mean square differences, and find the Average of all those.
Step 3 : Above found value is called Variance
Step 4 : Square root of Variance is Standard deviation of population.

Note: population means consider all of data instead of sample
```



![](images/standard_devi_formula.PNG)



Standard deviation measures the spread of a data distribution. **The more spread out a data distribution is, the greater its standard deviation.**

For example, the blue distribution on bottom has a greater standard deviation (SD) than the green distribution on top



![](images/spread_standard_deviation.PNG)

Interestingly, standard deviation **cannot be negative**. A standard deviation **close to 0 indicates that the data points tend to be close to the mean** (shown by the dotted line). The further the data points are from the mean, the greater the standard deviation.



[Mean and standard deviation versus median and IQR](https://www.khanacademy.org/math/statistics-probability/summarizing-quantitative-data/modal/v/mean-and-standard-deviation-versus-median-and-iqr)  [*Important* ]

- If you have pretty **symmetrical data set without any significant outliers**, that skewed the data **Mean and SD** can be pretty solid measurements.

- If you have data which is **skewed because of handful of data.** [ Example: if you have 1 outlier in data which is much larger due to which Mean and SD will be larger ] then **Median and IQR** will probably good measurements. 

  *Note : watch the video for explanation with example*

[Alternate variance formulas](https://www.khanacademy.org/math/statistics-probability/summarizing-quantitative-data/modal/v/statistics-alternate-variance-formulas) just for reference.





#### Variance and standard deviation of a sample. 

Note: It is **very much important** to understand difference between, population mean and sample mean, also variance and sample variance. Take a look at this section for best explanation.

So, while working with large set of data when we take samples for evaluation, how things gonna work.

[Sample variance](https://www.khanacademy.org/math/statistics-probability/summarizing-quantitative-data/modal/v/sample-variance) *



when we are measuring population variance from sample variance, it is more accurate to **divide by (n - 1)**

**instead of n.** [*note : n is called biased variance and (n-1) is called unbiased variance*]

check this section to understand why it so?

[More on standard deviation](https://www.khanacademy.org/math/statistics-probability/summarizing-quantitative-data/more-on-standard-deviation/v/another-simulation-giving-evidence-that-n-1-gives-us-an-unbiased-estimate-of-variance)

From above playlist [this video explains idea of bias](https://www.khanacademy.org/math/statistics-probability/summarizing-quantitative-data/modal/v/simulation-showing-bias-in-sample-variance) best with intuition. 

*note : this section provides how bias is important and how it affects the measurements* 



Then watch [Sample standard deviation and bias.](https://www.khanacademy.org/math/statistics-probability/summarizing-quantitative-data/modal/v/sample-standard-deviation-and-bias)



### Box and whiskers plot

![](images/box_plot.PNG)

Watch [this video](https://www.khanacademy.org/math/statistics-probability/summarizing-quantitative-data/modal/v/constructing-a-box-and-whisker-plot) for overall knowledge about box plots.

[Review of topic](https://www.khanacademy.org/math/statistics-probability/summarizing-quantitative-data/modal/a/box-plot-review) *

```
When to use? 
When you want to understand spread of distribution and Median for that distribution then Box and whiskers plot is very useful.
```



Percentile values and box plots

![](images/percentile_interpretation_with_5_values_box_plot.PNG)



### Judging outliers on bases of Interquartile ranges 

![](images/judging_outliers.png)



Statisticians agreed that anything **below 1.5 times IQR from Q1** and anything **above 1.5 times IQR from Q3** should be considered as outlier.  

See this [video](https://www.khanacademy.org/math/statistics-probability/summarizing-quantitative-data/modal/v/judging-outliers-in-a-dataset) for explanation.



### Mean absolute deviation (MAD)

[Video](https://www.khanacademy.org/math/statistics-probability/summarizing-quantitative-data/modal/v/mean-absolute-deviation)

The mean absolute deviation of a dataset is the average distance between each data point and the mean. It gives us an idea about the variability in a dataset.

```
Step 1: Calculate the mean.

Step 2: Calculate how far away each data point is from the mean using positive distances. These are called absolute deviations.

Step 3: Add those deviations together.

Step 4: Divide the sum by the number of data points.
```



![](images/mean_absolute_deviation.PNG)

## Modeling data distributions

[Percentiles](https://www.khanacademy.org/math/statistics-probability/modeling-distributions-of-data/modal/v/calculating-percentile)

![](images/percentile_2_ways.PNG)



Cumulative relative frequency graph [example](https://www.khanacademy.org/math/statistics-probability/modeling-distributions-of-data/modal/a/cumulative-relative-frequency-graph-problem)

#### What are z-scores?

A z-score measures **exactly how many standard deviations above or below the mean a data point is**.

[Explanation](https://www.khanacademy.org/math/statistics-probability/modeling-distributions-of-data/modal/a/z-scores-review) *important*

![](images/z_scores.PNG)



#### How parameters change as data is shifted and scaled?

[Watch this video : super useful.](https://www.khanacademy.org/math/statistics-probability/modeling-distributions-of-data/modal/v/analyzing-a-cumulative-relative-frequency-graph) 

And here is exercise *[important](https://www.khanacademy.org/math/statistics-probability/modeling-distributions-of-data/modal/a/transforming-data-problem)*



[This tutorial](https://www.sophia.org/tutorials/application-of-z-scores-2) shows practical use case of Z-score or Standard scores



### [Density curves](https://www.khanacademy.org/math/statistics-probability/modeling-distributions-of-data/modal/v/density-curves) 

**Area under density curve is always 1.0 or 100%** of the data points.

If frequency distribution is taken on Y axis then we can represent percentage of total distribution data fallen under specific range.

[Mean,Median and Skew](https://www.khanacademy.org/math/statistics-probability/modeling-distributions-of-data/modal/v/median-mean-and-skew-from-density-curves) in density curve.

![](images/mean_skew_in_density_curves.PNG)





### Normal distribution and empirical rule.

- Also known as Bell curve and Gaussian distribution. 

[Gentle intro to Normal distribution](http://onlinestatbook.com/2/normal_distribution/intro.html) 

![](images/normal_dostribution_area.PNG)

**Key points to note are **:

1. Normal distributions are **symmetric around their mean**.
2. The **mean, median**, and **mode** of a normal distribution **are equal**.
3. The **area under** the normal **curve is** equal to **1.0**.
4. Normal distributions are **denser in the center and less dense in the tails**.
5. Normal distributions are **defined by two parameters**, the **mean (μ) and the standard deviation (σ).**



**Empirical Rule is also referred as 68 - 95 - 99.7 rule.**

Check [this Video](https://www.khanacademy.org/math/statistics-probability/modeling-distributions-of-data/modal/v/ck12-org-exercise-standard-normal-distribution-and-the-empirical-rule) for example of calculating area using empirical rule

- **≈68%** of the data falls **within 1 standard deviation of the mean**
- **≈95%** of the data falls within **2** standard deviations of the mean
- **≈99.7%** of the data falls within **3** standard deviations of the mean



[Review of Normal distributions](https://www.khanacademy.org/math/statistics-probability/modeling-distributions-of-data/modal/a/normal-distributions-review) 

#### [Deep definition of the normal distribution](https://www.khanacademy.org/math/statistics-probability/modeling-distributions-of-data/modal/v/introduction-to-the-normal-distribution) * 



### Bi-Variate distributions and Scatterplots. *

The data distribution that take **2 variables** in consideration are called bi-variate distributions.

#### What is a scatterplot?

A scatterplot is a type of data display that shows the relationship between two numerical variables. Each member of the dataset gets plotted as a point whose (x, y) coordinates relates to its values for the two variables.



[Examples](https://www.khanacademy.org/math/statistics-probability/describing-relationships-quantitative-data/modal/v/bivariate-relationship-linearity-strength-and-direction) of linearity, Strength and direction of bivariate relationships. *

![](images/scatter_plots_relatoins.png)



> When we look at **scatterplot**, we should be able to describe the association we see between the variables.
>
> A quick description of the association in a scatterplot should always include a description of the ***form, direction,* and *strength* of the association**, along with the **presence of any *outliers*.**
>
> ***Form:*** Is the association linear or nonlinear?
>
> ***Direction:*** Is the association positive or negative?
>
> ***Strength:*** Does the association appear to be strong, moderately strong, or weak?
>
> ***Outliers:*** Do there appear to be any data points that are unusually far away from the general pattern?



> #### What are outliers in scatter plots?
>
> Scatter plots often have a pattern. We call a data point an **outlier** if it doesn't fit the pattern.



## Correlation coefficients *

How well a line can describe a relationship between both variables?

It is mostly represented by **" r "**

> The correlation coefficient r measures the direction and strength of a linear relationship

where **-1 <= r <= 1**

> Here are some facts about r:
>
> - It always has a value between −1 and 1.
> - Strong positive linear relationships have values of r closer to 1.
> - Strong negative linear relationships have values of r closer to −1.
> - Weaker relationships have values of r closer to 0.

[A very good example of calculating Correlation coefficients](https://www.khanacademy.org/math/statistics-probability/describing-relationships-quantitative-data/modal/v/calculating-correlation-coefficient-r) *



Some examples on how the data will be distributed with the coefficient values.

![](images/coeff_r_0.PNG)

![](images/coeff_r_0_5.PNG)

![](images/coeff_r_1.PNG)

![](images/coeff_r_minus_0_5.PNG)

![](images/coeff_r_minus_1.PNG)



### Trend lines

Trend lines describes the linear relation over trend in scatter plot. It shows if there is any trend in those scattered data points.

See [this](https://www.khanacademy.org/math/statistics-probability/describing-relationships-quantitative-data/modal/v/smoking-1945-extrapolation) example for quick explanation.



### Linear regression *

When we see a relationship in a data, we can **use a line to summarize the relationship in the data**. We can also **use that line to make predictions** in the data. This process is called linear regression.

[Example](https://www.khanacademy.org/math/statistics-probability/describing-relationships-quantitative-data/modal/v/fitting-a-line-to-data) 

Check this [Review notes](https://www.khanacademy.org/math/statistics-probability/describing-relationships-quantitative-data/modal/a/linear-regression-review) for more details. *



#### Residuals

The vertical distance between any data point and the regression line is called as residuals.

> A residual is a measure of how well a line fits an individual data point.

![](images/residual.PNG)



So, let's suppose that we have a person who is 60 inches tall and has weight of 100 pounds.

But from the Regression line which shows estimations, It is about 150. That difference is called Residual for any point. In this example it is **100 - 150 = -50.** 

> For data points above the line (r2), the residual is positive, and for data points below the line (r1), the residual is negative.

For best Estimations in statistics our goal is to **Fit the line to data in a way such that we can minimize all the residuals.** 



#### Least Squares regression. *

Calculating all the residuals and then minimize them all can be tricky process for there are some ways

- Take sum of all the absolute values of all the residuals.

But in statistics,

Instead of trying to find the Sum of all absolute values of residuals typical way of doing it is Taking Sum of all squares of residuals.

![](images/least_square.PNG)

So, instead of first equation we would follow second, Now As, Some of the data point which has higher negative or positive residual values they will contribute more in total.

For some data points which are very far from regression line, We can identify  them as outliers and after that we will try to minimize it.

We can adjust values of **slope(m) and y-intersection(b)** in equation of line **y = mx + b**, So that line will best fit to the data. and thus minimize the squared values of residuals.

This whole procedure is called Least squares regression. 

##### Explanation of these topics is [here](https://www.khanacademy.org/math/statistics-probability/describing-relationships-quantitative-data/modal/v/introduction-to-residuals-and-least-squares-regression) * 



#### Residual plots *

For all the residuals value we can create a plot and get below observations based on them,

consider this line which fits reasonably well to data, and it's residual plot.

![](images/residual_counting_1.PNG)



From the Residual plot of above data we can see that all residuals are scattered on both sides of axis and there is not any trend, If your residual plots are **evenly scattered and there is not any trend** then we can predict that **line fits pretty well to data.**





![](images/residual_counting_2.PNG)



Now, consider above example where the residual points are **not evenly scattered** and also **there is a non-linear relation / Trend in the residual plot**. And **Line is not fitting well to data**. and there are many outliers in the distribution.

Watch this Introductory video for [Residuals](https://www.khanacademy.org/math/statistics-probability/describing-relationships-quantitative-data/modal/v/residual-plots) 



#### R-squared intuition [Important]

[Read this 5 times](https://www.khanacademy.org/math/statistics-probability/describing-relationships-quantitative-data/modal/a/r-squared-intuition) **

Considering that we representing Co-relation coefficient by r.

 **r<sup>2</sup> measures how much prediction error is eliminated when we use least-squares regression.**

R-squared tells us what percent of the prediction error in the *y* variable is eliminated when we use least-squares regression on the *x* variable.

As a result, r<sup>2</sup> is also called the **coefficient of determination.** [Important]

[Watch this video](https://www.khanacademy.org/math/statistics-probability/describing-relationships-quantitative-data/modal/v/r-squared-or-coefficient-of-determination) for more info [Although it's confusing :p ]



##### Standard deviation of residuals or Root-Mean_squared error

From these both videos above here are some key points to remember,

- What a Root mean square error is simply average of all residuals's standard deviation, Or how many standard deviation away all points are from regression line.
- What R<sup>2</sup> : what % of variation in y is described by variation in x

See below image



![](images/r_squared_01.PNG)



Here Error in line or **SE<sub>Line</sub> (Squared error in line)** and **total variation in y from Mean is SE<sub>Y**</sub>

Question : How much of the variation in y is described by variation in x ? 

Let's derive it:



Now, SE<sub>Line</sub>  is the error, that means it is the total variation which is not described by the line
$$
SEline/SEy
$$
will describe How much of the variation in y is not described by variation in x.

Then we can subtract it from 1 to get our answer
$$
1 - (SEline/SEy)
$$


Which is also called r<sup>2</sup> or co-efficient of determination,

- If **r<sup>2</sup> is close to 1 regression line is good fit to the data**

  So when **SE<sub>line</sub> is smaller** in above equation r<sup>2</sup> will be near to 1.

- If **r<sup>2</sup> is close to 0 regression line is not good fit to the data**

  So when **SE<sub>line</sub> is larger** in above equation r<sup>2</sup> will be near to 0.



This SE<sub>line</sub> is also known as **Root-Mean-Squared error/distance [RMSD]**or Standard deviation of residuals

So, For a better fit the RMSD should be smaller.

Consider watching [this video](https://www.khanacademy.org/math/statistics-probability/describing-relationships-quantitative-data/modal/v/standard-deviation-of-residuals-or-root-mean-square-error-rmsd) for RMSD explanation 

> TIP
>
> Watch both videos describing r<sup>2</sup> and RMSD twice to understand whole thing



[This Extra full playlist](https://www.khanacademy.org/math/statistics-probability/describing-relationships-quantitative-data#more-on-regression) consists all the things and examples regarding regression  *



### [Probability](probability.md)



------



#### Notes:

I have taken notes from Khan academy playlist but I would argue that you can learn better from this Udacity Free course then anywhere else

Stats101 : [check it out here](https://classroom.udacity.com/courses/st101) 

Descriptive statistics : [check it out here](https://classroom.udacity.com/courses/ud827)

