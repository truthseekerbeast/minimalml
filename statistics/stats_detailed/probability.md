# Probability 

#### Probability is simply how likely something is to happen.

Whenever we’re unsure about the outcome of an event, we can talk about the probabilities of certain outcomes—how likely they are. 

The analysis of events governed by probability is called statistics.

**Probability of an event = (# of ways it can happen) / (total number of outcomes)**

Check basics [here](https://www.khanacademy.org/math/statistics-probability/probability-library/modal/a/probability-the-basics)

Monty Hall problem [Intuition](https://www.khanacademy.org/math/statistics-probability/probability-library/modal/v/monty-hall-problem) *



### Sets

A group of things are called set

Example : A = {1 ,3 ,5} , B = {1 ,12, 5, 6}

- Union of sets  = A U B = {1 ,3 ,12 , 5 ,6}

- Intersection of sets = {1 ,5}



##### Relative compliment or difference between sets

**A - B** = { 3 } = **A \ B**

Set of all elements in A which is not in B : Basically remove all elements from A which is in B.

Which is called relative compliment as We are relatively comparing sets and doing difference.

Now If I remove anything from any set which is not part of any other sets relatively.



Universal set : **U**

Don't get confused in Union sign and this U.

**Universal set is set of all possible elements for our use case** 



Some Pre-defined Universal sets are,

**R** - Set of all the real numbers

**Z** - Set of all the integers

**Q** - Set of all fractional numbers



So For our set A =  {1 ,3 ,5} , absolute complement of A or A' is 

U - A = U \ A = All the element in universe which are not in A.



#### [Subsets, Supersets and Strict subsets](https://www.khanacademy.org/math/statistics-probability/probability-library/modal/v/subset-strict-subset-and-superset) *

A = {1, 3, 4, 6, 67}

B = {1, 3, 6}

- B is Subset of A, So all values of B are in A
- B is also Strict subsets as All values of B are in A but A also contains other values then B.

- A is superset of B
- A is also strict superset of B

[Set operations example](https://www.khanacademy.org/math/statistics-probability/probability-library/modal/v/bringing-the-set-operations-together)



#### [Theoretical VS Experimental probability](https://www.khanacademy.org/math/statistics-probability/probability-library/modal/v/comparing-theoretical-to-experimental-probabilites) *  

Also check [this](https://www.khanacademy.org/math/statistics-probability/probability-library/modal/v/experimental-versus-theoretical-probability-simulation) simulation of same topic.



#### Addition Rule

**P(A OR B) = P(A) + P(B) - P(A AND B)** 

When you are counting probability of 2 sets you need to always subtract the overlapping area because It will be repeated twice for both sets.



![](images/mutually_exclusive_events.png)

If two sets are completely independent then any events that includes both of them will be mutually exclusive events.

[Link](https://www.khanacademy.org/math/statistics-probability/probability-library/modal/v/addition-rule-for-probability)



#### Compound events and sample spaces

> For A coin there are 2 possibilities H or T
>
> So Sample Space would be  {H ,T}



So sample space basically contains all the possible output values for any given events.

Compound events are those where probability of picking any values from given sets are dependent on more than one factor.

```
For example, In a backery which sells cupckaes there are 3 flavors: Vanila, Strawberry and Chocolates.
Also, they comes in 3 sizes: Small, Medium and Large
So picking up a cupcake from a backery can have possible 9 values, that can be represented by below image:
```

![](images/sample_space.png)



```
Here picking up any random cupcake depends on all values from above tables, 
So choosing any cupcake depends on 2 factors that are flavor and size
These are called compound events.
```

Also see [this example](https://www.khanacademy.org/math/statistics-probability/probability-library/modal/v/probability-from-compound-sample-space) for compound event



#### Compound probability of independent events

```
Example of independent events:
- Flipping a coin
As, There is no relationship between a next result of flip with the current result.
```

Now, For counting probability of independent events is multiplying probabilities of individual events

```
Let's say we need to count probability P(H H) or 2 consicutive heads.
So, all the probabilities or combinitions are here
H H
H T
T H
T T
So, there are total 4 ways event can occur, Now from that we are only interested in 1 type of event so 
P = 1 / 4

We can say this is equal to multiplication of probabilities of individual events
(1/2)*(1/2) = 1/4
```

Which is called compound probabilities of independent events.



