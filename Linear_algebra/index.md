# Algebra



## [Algebra part -1](https://www.khanacademy.org/math/algebra) 

### [Introduction to Co-ordinate plane](https://www.khanacademy.org/math/algebra/introduction-to-algebra/overview-hist-alg/v/descartes-and-cartesian-coordinates) 

**Linear Equation** : **y = 2X-1**

- Y is dependent variable As it's value depends on value of X

- X is independent variable

- If we take 2-dimensional plan having 2 Axis where **vertical represent values of dependent variable** and **horizontal represents Independent variable** then for **every value of X and Y there is one point on the plan [Co-ordinate]** and If we **connect all the points It will represent a line**.

- This image below represents relation between Algebra and Geometry

  ![](image/01_algebra_geometry.PNG)



*There is a reason behind calling it a linear equation because if you plot this points on Euclidian plane and connect all of them then It will represent a line.*

[Another Definition of linear equation](https://youtu.be/AOxMJRtoR2A?t=435)

The equation is called linear equation if  

1. Every term either going to be a constant **OR**
2. It is constant times variable to the power of one [X , Y and not X^2 ] 
3. And you are not multiplying/dividing your variables to each other [ X*Y or  X/Y ]



------

### [Variable](https://www.khanacademy.org/math/algebra/introduction-to-algebra/modal/v/what-is-a-variable)

Variable is just the **symbol in equation** that **represent varying value**.

Example : 10 + X  = 20

Example : 10 + 😲 = 20



------

### [Intercepts](https://www.khanacademy.org/math/algebra/two-var-linear-equations/modal/v/x-and-y-intercepts)

**X intercept : When y = 0**

**Y intercept : When x = 0**

![](image/02_x_y_intercepts_for_lines.PNG)

*This is again how linear algebra is relate to Geometry, here both lines intersects at (6,0) which is also solution for both equations satisfies for values X = 6 and Y = 0*



------

### [Slope](https://www.khanacademy.org/math/algebra/two-var-linear-equations/modal/v/introduction-to-slope)

**change in Vertical  / change in horizontal**

![](image/03_slope.PNG)



**Positive and Negative slope:** If changing one direction changes other direction positively then, slope will be positive and If negatively then slope will be negative. 

**Line always has constant value for slope**

![Slope simple definition](image/03_01_slope.PNG)

[Quick review of Topic here](https://www.khanacademy.org/math/algebra/two-var-linear-equations/modal/a/slope-review)

[Horizontal and Vertical line explained](https://www.khanacademy.org/math/algebra/two-var-linear-equations/modal/v/examples-of-slopes-and-equations-of-horizontal-and-vertical-lines) 



[Slope-Intercept form of Linear equation](https://www.khanacademy.org/math/algebra/two-var-linear-equations/modal/a/introduction-to-slope-intercept-form) **[ y = mx + b ]**

Example: y = 2x + 3

y = some constant time x to the first power **[co-efficient is slope]** **+** some other constant **[y-intercept]** 

[Standard form of linear equation](https://www.khanacademy.org/math/algebra/two-var-linear-equations/modal/v/standard-form-for-linear-equations) **[ ax + by = c ]**



------

### [Functions](https://www.khanacademy.org/math/algebra/algebra-functions/modal/v/what-is-a-function)

**Input** --------> [**Logic**] ----------> **Output**

**It should be exactly one output for given input** 

Example: **y = f(X) = 3x + 5** 

*Function is something which takes input values and according to that it provide specific output value.*



**[Equations Vs functions](https://www.khanacademy.org/math/algebra/algebra-functions/modal/v/difference-between-equations-and-functions)**

Where equation defines something like x + 3 = 10 where there is no relationship between variables, Functions are something which defines relationship between 2 or more variables like 4x + 2y = 9, There are some equations which works like a function.

**Domain of the function** is Set of all valid inputs for that function definition 
**Range of the function** is set of all possible output values.



* [Absolute minima [minimum point] and absolute maxima [maximum point]](https://youtu.be/xmrhZ5ySaD0?t=175)
  Combined it's called absolute extrema

![](image/04_function_absolute_min_max.png)



* [Relative/Local minima [minimum point] and maxima [maximum point]](Relative/Local minima [minimum point] and maxima [maximum point]) * 
  Combined it's called relative extrema

![](image/04_function_relative_min_max.PNG)

- if at any point if it's value is **maximum then some nearest range of points**, It's Local maxima
- if at any point if it's value is **minimum then some nearest range of points,** It's Local minima



[When Function is said positive/negative and increasing/decreasing](https://www.khanacademy.org/math/algebra/algebra-functions/modal/v/increasing-decreasing-positive-and-negative-intervals)

if  Y = f(X)

Positive : For all intervals when Y > 0 

Negative: For all intervals when Y < 0

Increasing: For all intervals when X is increasing Y is also Increasing.

Decreasing: For all intervals when X is increasing Y is decreasing.

![](image/04_function_positive_negative_increasing_decreasing.png)



**[Piecewise function](https://www.khanacademy.org/math/algebra/absolute-value-equations-functions/modal/v/piecewise-function-example)**

Functions that are defined piece by piece.

if for ranges of X the value of f(X) changes, It's called piecewise function.

![](image/04_function_stepwise.png)



------

### [Exponential growth](https://www.khanacademy.org/math/algebra/introduction-to-exponential-functions#exponential-vs-linear-growth-over-time)

*something* <sup>x</sup>  | example : 3<sup>x</sup> 

When **exponent is variable**. So, growth will directly affected by it's value multiplication



[Simplifying Exponential expressions](https://www.khanacademy.org/math/algebra2/exponential-growth-and-decay-alg-2/modal/v/simplifying-an-exponential-expression).



------



### [Polynomials](https://www.khanacademy.org/math/algebra/introduction-to-polynomial-expressions/modal/v/polynomials-intro)

Many terms

Sum of finite terms : Where each term can be represent in following form,

**A . X<sup>n</sup>**

Where **A is coefficient**, **x is variable** and the **power** of that variable **must be non-negative integer.**

[Synthetic division of polynomials](https://www.khanacademy.org/math/algebra2/arithmetic-with-polynomials/modal/v/synthetic-division)

Also, [Polynomial remainder theorem](https://www.khanacademy.org/math/algebra2/arithmetic-with-polynomials/modal/v/synthetic-division) is useful when you only want remainder of polynomial division.



------



### [Quadratics](https://www.khanacademy.org/math/algebra/quadratics)

**Polynomial expressions which are of second degree** are called as Quadratics, Example: X<sup>2</sup> + 5x + 2

[Method](https://www.khanacademy.org/math/algebra/polynomial-factorization/modal/a/factoring-quadratics-leading-coefficient-not-1) of finding [Factors](https://www.khanacademy.org/math/algebra/polynomial-factorization/modal/v/factors-and-divisibility-in-algebra) of Quadratic Equations.

**Perfect squares**: Expressions like ( x<sup>2</sup> + 6x + 9 ) -> Where both factors are (x + 3) are called perfect square equations.

![](image/05_quadratics_perfect_squares.PNG)

Here **first and last terms can be represent as squares of something** and by **multiplying them we can get middle expression.**

------



### [Parabola](https://youtu.be/BGz3pkoGPag)

- Vertex : Means **minimum or maximum point** of parabola.
- Parabola can be **open upwards or open downwards**.
- Axis of symmetry **is line over which you can flip the graph so parabola meets/folds on to itself**. 
- **Axis of symmetry passes directly through vertex**
- **Quadratic equations** generally represented as **parabolas**



![](image/05_quadratics_parabolas.png)

if coefficient of x<sup>2</sup> is positive, it's upward opening parabola if negative then downward.



> [Graphing Quadratic equations](https://www.khanacademy.org/math/algebra/quadratics/modal/v/graphing-quadratics-in-factored-form)

>  [Quadratic formula](https://www.khanacademy.org/math/algebra/quadratics/modal/v/using-the-quadratic-formula)

![](image/05_quadratics_formula.PNG)



**[Nature of roots](https://www.toppr.com/guides/maths/quadratic-equations/nature-of-roots/) of quadratics equations**

| Value of [discriminant](https://www.khanacademy.org/math/algebra/quadratics/modal/a/discriminant-review):  b<sup>2</sup> -4ac |       Nature of roots        |
| :----------------------------------------------------------- | :--------------------------: |
| * b<sup>2</sup> – 4ac = 0                                    |        Real and equal        |
| * b<sup>2</sup>– 4ac < 0                                     |    Unequal and Imaginary     |
| b<sup>2</sup> – 4ac > 0 (is a perfect square)                |  Real, rational and unequal  |
| b<sup>2</sup> – 4ac > 0 (is not a perfect square)            | Real, irrational and unequal |
| b<sup>2</sup> – 4ac > 0 (is a perfect square and a or b is irrational) |          Irrational          |
| * b<sup>2</sup> – 4ac > 0                                    |       Real and unequal       |



[Proof](https://www.khanacademy.org/math/algebra/quadratics/modal/v/proof-of-quadratic-formula) of quadratic equation (Important) 
Also in this video watch carefully the method of converting equation to it's perfect square form, 

Perfect square conversion: **Take half of coefficient of middle term and then square of it would be last term.**



[Different forms of quadratic equations](https://www.khanacademy.org/math/algebra/quadratics/modal/v/strategy-with-forms-of-quadratics) and it's usefulness (Use cases of When to use which?)

[finding vertex and axis of symmetry](https://www.khanacademy.org/math/algebra/quadratics/modal/v/quadratic-functions-2) of parabola



------



## [Algebra part 2](https://www.khanacademy.org/math/algebra2)

### [Composition of functions](https://www.khanacademy.org/math/algebra2/manipulating-functions/modal/a/introduction-to-function-composition)

- Calling function from function or applying chains of functions.
  Example: *f (g(2) )*

So First we will find what is *g(2)* and then we will pass this output value to function *f* 



**Imaginary number:**

i where *i<sup>2</sup> = -1*

### [Complex numbers](https://www.khanacademy.org/math/algebra2/introduction-to-complex-numbers-algebra-2/modal/v/complex-number-intro)

Composition of real number and Imaginary numbers like *3 + 5i* 

To represent complex number plot **real part** of number on *x Axis* and **Imaginary part** on the *y axis*

We need imaginary numbers when we have some negative number's square root.



### [Logarithms](https://www.khanacademy.org/math/algebra2/exponential-and-logarithmic-functions/modal/v/logarithms)

Logarithm is used to represent a **value of power of some number**, raising by which you will get another number or answer.

Example:

if we say that **what is the power of 2 that is equal to 64,** We can write this in following way

1. 2<sup>x</sup> = 64

2. Or else in logarithmic notation, log<sub>2</sub><sup>64</sup>  = x

So, both are correct way and thus, **2<sup>x</sup> = 64** is same as **log<sub>2</sub><sup>64</sup>** 



![](image/06_logarithms_examples.PNG)

Note: **Exponentials and Logarithms are inverse functions of each other**, So in graphical representations it will always represent reflection of existing function.



[The Magical number e](https://www.khanacademy.org/math/algebra2/exponential-and-logarithmic-functions/modal/v/e-as-limit)

![](image/06_logarithms_definition_of_e.PNG)



When you take logarithm of any number taking base ***e*** it's call **natural logarithm** or **ln**

Example: log<sub>e</sub><sup>64</sup>  = ln64 = log <sub>2.718</sub><sup>64</sup> 

[Nature of Graphs](https://www.khanacademy.org/math/algebra2/exponential-and-logarithmic-functions/modal/v/comparing-exponential-logarithmic-functions) of Logarithms and Exponentials.



------

### [Trigonometry](https://www.khanacademy.org/math/algebra2/trig-functions)



#### [Degrees and radians](https://www.khanacademy.org/math/algebra2/trig-functions/modal/v/introduction-to-radians)

![](image/07_trigonometry_radians_basics.png)



**Important formulas for conversions between radians and degrees**

![](image/07_trigonometry_radians.png)





#### [Unit circle definition](https://www.khanacademy.org/math/algebra2/trig-functions/modal/v/unit-circle-definition-of-trig-functions-1) **of Sin, cosine and tan theta [important]**

![](image/07_geometry_sine_cosine_tan.png)



- **Terminal side** is a side with respect to angle is created with positive x axis (or, initial side)

- **Positive angle** will be created if we move in **counter clock-wise direction** with respect to initial side.

- **Negative angle** will be formed if we move in **clock-wise direction** with respect to initial side.



*sine  = y co-ordinate where terminal side of the angle intersects the unit circle*

*cosine  = x co-ordinate where terminal side of the angle intersects the unit circle*

*tangent = sine/cosine = y co-ordinate/ x co-ordinate*



- [Graph](https://www.khanacademy.org/math/algebra2/trig-functions/modal/v/we-graph-domain-and-range-of-sine-function) of sine functions
- [Grpah](https://www.khanacademy.org/math/algebra2/trig-functions/modal/v/we-graphs-of-sine-and-cosine-functions) of cosine functions and intersections of sine and cosine graphs.
- [Grpah](https://www.khanacademy.org/math/algebra2/trig-functions/modal/v/tangent-graph) of tangent functions



[sinusodial functions features](https://www.khanacademy.org/math/algebra2/trig-functions/modal/v/midline-amplitude-period) 

