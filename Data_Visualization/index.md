# Data Visualization



### Top note:

For examples of any type of graph and basic techniques and methods, Best to use Python graph gallery where, [All types of graphs visualized](https://python-graph-gallery.com/) with Python code in matplotlib and other libraries - *most useful link* 



### Libraries to Go with step by step

1. MatplotLib ( Core library on based of which some major libraries have been created)
2. **SeaBorn** [ I have started with this and so should you]
3. Pandas [ For some basic plotting this this is also useful ] , [All about pandas Viz](http://pandas.pydata.org/pandas-docs/version/0.18.0/visualization.html)

For interactive visualization,

1. Bokeh
2. **Plotly**

Also for some advance level visualization checkout,

1. HoloViews 
2. Altair

When you have very big dataset then go for,

1. DataShader





### SeaBorn cheat sheet

![](images/Seaborn_Cheatsheet_Datacamp.png)





### How to choose chart for any data?

![](images/chart_choice_guide_ultimate.png)





#### Here is another simple chart selection guide based on this article which they called as [ultimate guide to data visualization.](https://www.analyticsvidhya.com/blog/2015/05/data-visualization-resource/)

![](images/Chart_Selection.jpeg)





#### [This blog](https://towardsdatascience.com/february-edition-data-visualization-18e1b663edc4) contains links to great articles for data visualization 



#### Video resources

- [What tools are available for visualization in python?](https://www.youtube.com/watch?v=FytuB8nFHPQ)

- [Some basic charting with SeaBorn](https://www.youtube.com/watch?v=KvZ2KSxlWBY) and It's [Github](https://github.com/StephenElston/ExploringDataWithPython) link.

- [Some advance visualizations](https://www.youtube.com/watch?v=ms29ZPUKxbU&t=6062s) with altair and vega. 



### Notes: 

##### When we just start with data exploration first we need to know whether variable is of categorical type or continuous?

- For **continuous variables**, We look for Central tendency, Spread of data and Outliers.
- For **Categorical variables** we look at frequency table 

For these type of analysis 

- Histogram and

- Box plots 

  are most commonly used visualization techniques.

##### For comparisons

- For categorical variables go with **Bar** charts
- Over time  or Continuous variables go with **Line** charts
- If there are multiple categories, it is a good practice to segregate categories in different groups and then compare accordingly. **Decision Tree** is one of the useful visualization technique to explore data.

##### For Relationship

- It is widely used to understand the correlation between two or more continuous variables. Most common method to visualize this information is **Scatter Plot**.
- We can also add third variable in scatter plot by using size of the points (known as **Bubble Chart**) 