# Python

[Cheat sheet that contains all concepts that are basics and required for Machine Learning](https://www.kaggle.com/learn/python)

[Run python in browser, online python interface](https://repl.it/@TsBeast/RoyalblueMountainousDictionary)

[Best book for learning python with hands on examples](https://automatetheboringstuff.com/)

[Python Official documentation](https://docs.python.org/3/)



### Topic in python3

[All about python Object oriented programming](https://jeffknupp.com/blog/2014/06/18/improve-your-python-python-classes-and-object-oriented-programming/)



### All best [books for Python in PDF format](https://drive.google.com/open?id=130LWPHdAE0T52iaIYSTx7Ys9zviVUNhL)


