# Seaborn notes



**Figure level functions**

1. relplot()
2. catplot()



### relplot()

This is a [figure-level function](https://seaborn.pydata.org/introduction.html#intro-func-types) for visualizing statistical relationships using two common approaches: scatter plots and line plots. [`relplot()`](https://seaborn.pydata.org/generated/seaborn.relplot.html#seaborn.relplot) combines a [`FacetGrid`](https://seaborn.pydata.org/generated/seaborn.FacetGrid.html#seaborn.FacetGrid) with one of two axes-level functions:

1. scatterplot()
2. lineplot()



Common import and setting up style

```
import seaborn as sns
sns.set(style="darkgrid")
- None, or one of {darkgrid, whitegrid, dark, white, ticks}
```



Most basic plotting, by default scatter plot.

```
sns.relplot(x="total_bill", y="tip", data=tips);
```



