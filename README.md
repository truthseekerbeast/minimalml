# MinimalML

**Step by Step Machine learning.**



### 1. [Python](python)

### 2. [Algebra](Linear_algebra) 

### 3. [Data Visualization](Data_Visualization)

### 4. [Statistics and Probability Theory](statistics)

### 5. Machine Learning [Topics]() and [Terms]()







# Major things to focus



Data manipulation in... 

### [Pandas](pandas)

### [Num-py]()



Data visualization in... 

### [MatplotLib](https://www.youtube.com/watch?v=UO98lJQ3QGI&list=PL-osiE80TeTvipOqomVEeZ1HRrcEvtZB_)

### [Seaborn]()

### [Plotly]()



Ready to use Machine learning libraries

### [Scikit-learn]() also known as sklearn



For Deep learning learn this packages.

### [Keras]() and [Tensorflow]()





# Other useful tools that are useful but not in python stack.



### Excel

### Tableau





# Cheat sheets for every topics

1. Data visualization
   - [Chart choice guide](cheatsheets/chart_choice_guide_ultimate.png)
   - [Seaborn cheat sheet](cheatsheets/Seaborn_Cheatsheet_Datacamp.png)
   - [Matplotlib and Seaborn data visualization cheat sheet.](cheatsheets/data_viz_seaborn_matplotlib.jpg)

2. Data exploration
   - [Using Numpy, Pandas and Matplotlib](cheatsheets/data_exploration.jpg)
   - [Using pandas](cheatsheets/data_exploration_cheatsheet.jpg)
3. [Numpy](cheatsheets/Numpy_Python_Cheat_Sheet.pdf)
4. [Pandas](cheatsheets/pandas_cheat_sheet.pdf)
5. [Python](cheatsheets/PythonForDataScienceCheatsheet.pdf)
6. [Probability](cheatsheets/probability_cheatsheet_cheat_sheet.pdf)
7. [Sql](cheatsheets/sql-cheat-sheet.pdf)
8. ML algorithms
   - [Ml algorithms code cheat sheet](cheatsheets/ml_algorithms_code_cheat_sheet.webp)
   - [sklearn's guide of algorithm choice](cheatsheets/sklearn_algo_guide.png)
9. [Sklearn methods](cheatsheets/sklearn_methods_infographic.pdf)



# Links to useful things

Check out the most important links are [here](links.md)







