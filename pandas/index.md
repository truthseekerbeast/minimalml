1. Pandas Crash [course on Kaggle](https://www.kaggle.com/learn/pandas) and other [Complete course](https://www.kaggle.com/themenyouwanttobe/complete-tutorial-of-pandas-python) which contains all major operations you can perform in pandas

2. [Dataframe](https://towardsdatascience.com/pandas-dataframe-a-lightweight-intro-680e3a212b96) and [Series](https://towardsdatascience.com/pandas-series-a-lightweight-intro-b7963a0d62a2) in Pandas.

3. Pandas exercises are available in [this repo](https://github.com/TruthSeekerBeast/pandas_exercises.git)

4. [This blog](https://towardsdatascience.com/23-great-pandas-codes-for-data-scientists-cca5ed9d8a38)  contains all basic things you do in pandas day to day.



Note:

#### [This is Everything you need to master pandas](https://medium.com/reversebits/everything-you-need-to-master-pandas-966e4b24d82d)

