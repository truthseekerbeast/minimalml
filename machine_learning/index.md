ML algorithms are divided in 2 major categories

1. **Supervised ML algorithms**

   1. Regression problems [Where you need continuous value output]
   2. Classification problems [Discrete value output]

   Examples:

   **Regression** - Given a picture of a person, we have to predict their age on the basis of the given picture

   **Classification** - Given a patient with a tumor, we have to predict whether the tumor is malignant or benign.

2. **Unsupervised ML algorithms**

   Unsupervised learning allows us to approach problems with little or no idea what our results should look like. We can derive structure from data where we don't necessarily know the effect of the variables.

   Examples:

   **Clustering:** Take a collection of 1,000,000 different genes, and find a way to automatically group these genes into groups that are somehow similar or related by different variables, such as lifespan, location, roles, and so on.

   **Non-clustering:** The "Cocktail Party Algorithm", allows you to find structure in a chaotic environment. 

   (identifying individual voices and music from a mesh of sounds at a [cocktail party](https://en.wikipedia.org/wiki/Cocktail_party_effect)).



The tree of all available algorithms can be categorized as below



![](images/ml_algo_tree.png)





### `Some Basics`

Let's suppose we are predicting sales value with different advertising. Now, If we can determine various relations between different advertising strategies effect on sales then we can instruct client to set budget accordingly.

1. Here, `advertising budget` is an **input variable**

Basically denoted by **X** with subscript for different types. For example, TV budget -> X1, Radio budget -> X2

Also, called as predictors, features, **independent** variables.

2. And `Sales` is an **output variable**

Basically denoted by **Y**, Also called as result, **dependent** variable.









